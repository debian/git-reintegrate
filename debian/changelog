git-reintegrate (0.4-3) UNRELEASED; urgency=medium

  * Apply multi-arch hints.
    + git-reintegrate: Add :any qualifier for ruby dependency.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update pattern for GitHub archive URLs from /<org>/<repo>/tags
    page/<org>/<repo>/archive/<tag> -> /<org>/<repo>/archive/refs/tags/<tag>.
  * debian/watch: Use GitHub /tags rather than /releases page.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 10 Jul 2021 14:01:31 -0000

git-reintegrate (0.4-2) unstable; urgency=medium

  * Include patches from my still-unresponded 2018 PRs:
    * Fix handling of options to "merge" command
    * Fix "git merge-base" invocation
    * Mention reintegrations as such in reflog
    * Make reintegrate operations stand out in output
  * Update Vcs-* for salsa.
  * Use https for copyright format URI (lintian).
  * Switch to debhelper-compat 12.

 -- Yann Dirson <dirson@debian.org>  Sat, 02 Jan 2021 16:51:16 +0100

git-reintegrate (0.4-1) unstable; urgency=medium

  * New upstream release.

 -- Yann Dirson <dirson@debian.org>  Fri, 03 Jun 2016 23:56:08 +0200

git-reintegrate (0.3-2) unstable; urgency=medium

  * Require ruby >= 1.9.3.
  * Fixed Vcs-Browser URL.
  * Mention in debian/copyright that test/sharness is GPL-2.0+, not
    GPL-2.0 (thanks Thorsten Alteholz).

 -- Yann Dirson <dirson@debian.org>  Mon, 28 Jul 2014 22:18:56 +0200

git-reintegrate (0.3-1) unstable; urgency=low

  * Initial release (Closes: #755957).

 -- Yann Dirson <dirson@debian.org>  Thu, 24 Jul 2014 22:39:08 +0200
